window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-sass');

} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

// qs untuk stringify JSON axios POST
window.qs = require('qs');

// highchart
window.Highcharts = require('highcharts');

let baseURL = document.head.querySelector('meta[name="base-url"]').content;

window.AjaxUrl = {
	// // Mahasiswa Per Angkatan
	// MhsPerangkatanInitialData: baseURL + 'api/get/mhs-angkatan/initial-data',
	// MhsPerangkatanFilterData: baseURL + 'api/post/mhs-angkatan/filter',
	// // Mhs Masuk dan Lulus
	// MhsMasukNLulusInitialData: baseURL + 'api/get/mhs-masuk-dan-lulus/initial-data',
	// MhsMasukNLulusFilterData: baseURL + 'api/post/mhs-masuk-dan-lulus/filter',

	KuesionerInitialData: baseURL + 'KuesionerMhs/initial_data',
	KuesionerPostData: baseURL + 'KuesionerMhs/post_data',
	
	LaporanInitialData: baseURL + 'KuesionerPetugas/initial_data'
};