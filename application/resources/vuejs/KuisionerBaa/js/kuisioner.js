/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
import Vue from 'vue'
// import Vuetify from 'vuetify'
 // import 'vuetify/dist/vuetify.min.css'

 
import { Line } from 'vue-chartjs'

// Vue.use(Vuetify)
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)

Vue.component('kuisioner-content',require('./components/kuisioner/Kuisioner.vue'))
Vue.component('laporan-content',require('./components/kuisioner/Laporan.vue'))


const app = new Vue({
	el: '#kuisioner'
})

