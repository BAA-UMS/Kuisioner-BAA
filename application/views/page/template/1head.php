<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="base-url" content="<?php echo base_url() ?>">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url('assets/images/system/ums-icon.png') ?>">
    <title><?php echo $title ?> |BAA-UMS</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/custom/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<!--     <link href="<?php echo base_url('assets/custom/bootstrap/dist/css/bootstrap-select.min.css'); ?>" rel="stylesheet"> -->
    <!-- Menu CSS -->
    <!-- <link href="<?php echo base_url('assets/custom/css/sidebar-nav.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/custom/css/tablesaw.css'); ?>" rel="stylesheet"> -->
    <!-- animation CSS -->
    <link href="<?php echo base_url('assets/custom/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url('assets/custom/css/animate.css'); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/custom/css/style.css'); ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url('assets/custom/css/colors/default.css'); ?>" id="theme" rel="stylesheet">
    <!-- emoji -->
    <link href="<?php echo base_url('assets/custom/css/emoji.css'); ?>" rel="stylesheet">
    <!-- Kuesioner -->
    <link href="<?php echo base_url('assets/css/kuisioner.css'); ?>" id="theme" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- [if lt IE 9]> -->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!-- <![endif] -->
</head>