<?php $this->load->view('page/template/1head') ?>
<?php $this->load->view('page/template/2menu') ?>

    <!-- content -->
    <div id="page-wrapper">
	    <div class="container-fluid">
	        
	        <!-- ============================================================== -->

	        <!-- .row -->
	        <br>
	        <laporan-content></laporan-content>
	        <!-- /.row -->
	    </div>
	</div>

    <!-- endcontent -->

<?php $this->load->view('page/template/3foot') ?>