<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KuesionerMhs extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->model('KuesionerMhsModel');
		//Do your magic here
	}

	public function index()
	{
		$data=[
			'title' => "Kuesioner",
		];

		$this->load->view('page/content/Kuesioner', $data);
	}

	public function initial_data()
    {        
    	$nim	= "L200140162";
    	$jawaban = [];
    	$Esay1 = '';
    	$Esay2 = '';
    	// jika mhs pernah mengisi maka diisikan sesuai dengan data
    		// $jawaban = $this->model->
    		// $Esay1 = ''
    		// $Esay2 = ''
    	//

        // ======================== OUTPUT =========================== //
        $result = [
            'success' => true,
            'result'  => [
            	"nim" =>$nim,
            	"pertanyaan" => [
            		['id_soal'=>1,'soal'=>'Kejelasan prosedur layanan'],
		            ['id_soal'=>2,'soal'=>'Kecepatan Layanan'],
		            ['id_soal'=>3,'soal'=>'Ketepatan Layanan'],
		            ['id_soal'=>4,'soal'=>'Kejelasan Informasi layanan'],
	            	['id_soal'=>5,'soal'=>'Keramahan petugas'],
	            	['id_soal'=>6,'soal'=>'Kesediaan Petugas Membantu'],
	        	    ['id_soal'=>7,'soal'=>'Penampilan Petugas']
        		],
                "jawaban" => $jawaban,
                'Esay1' => $Esay1,
                'Esay2' => $Esay2,
            ]
        ];

        // $this->response($result, 201);
        return response($result,201);
    }
    public function post_data()
    {
    	$data= $this->input->post();
    	$const= 0;
        
        print_r($data);die();
        foreach ($data as $asd) {
            if ($const>0){
                echo $asd;
            }
        }
        die();
        
        $jawaban = array(
                        'nim' => '',
                        'question_id' => '',
                        'answer_id' => '', 
                    );
        $this->KuesionerMhsModel->post_jawaban($jawaban);


    	$result = [
            'success' => true,
            'result'  => [
            	"redirect" => base_url(),
            	"keterangan" => "Terimakasih sudah mengisi",
            ]
        ];

        // $this->response($result, 201);
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));

    }

}

/* End of file Controllerdata.php */
/* Location: ./application/controllers/Controllerdata.php */