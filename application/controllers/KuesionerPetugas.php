<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KuesionerPetugas extends CI_Controller {

	public function index()
	{
		
		$data=[
			'title' => "Laporan",
		];

		$this->load->view('page/content/KuesionerLaporan', $data);

	}
	public function initial_data()
    {        

    	$kategori1=0;
    	$kategori2=0;
    	$kategori3=0;
    	$kategori4=0;
    	$kategori5=0;
    	//================== mengambil data dari model/db=========
    	// $kategori1=$this->model->;
    	// $kategori2=$this->model->;
    	// $kategori3=$this->model->;
    	// $kategori4=$this->model->;
    	// $kategori5=$this->model->;

        // ======================== OUTPUT =========================== //
        $result = [
            'success' => true,
            'result'  => [
            	'data'=>[$kategori1, $kategori2, $kategori3, $kategori4, $kategori5]
            ]
        ];

        // $this->response($result, 201);
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

}

/* End of file KuesionerPetugas.php */
/* Location: ./application/controllers/KuesionerPetugas.php */