const mix = require('laravel-mix');

mix.js('application/resources/vuejs/KuisionerBaa/js/kuisioner.js', 'assets/js')
	.sass('application/resources/vuejs/KuisionerBaa/sass/kuisioner.scss', 'assets/css')
	.setPublicPath('./');
